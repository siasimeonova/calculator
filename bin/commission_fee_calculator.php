#!/usr/bin/env php
<?php

use App\CommissionFeesImporter;

require '../vendor/autoload.php';


(new CommissionFeesImporter())->importData($argv[1]);
