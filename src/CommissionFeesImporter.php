<?php


namespace App;

use App\Account\LegalPersonAccount;
use App\Account\NaturalPersonAccount;
use App\Transaction\CashInTransaction;
use App\Transaction\CashOutTransaction;
use DateTime;

class CommissionFeesImporter
{

    private $legalAccounts = [];
    private $naturalAccounts = [];

    public function importData($filePath)
    {
        $fileHandle = $this->openCsv($filePath);
        if (!$fileHandle) {
            return 'INVALID FILE !!!';
        }

        while (($data = fgetcsv($fileHandle, 1000, ",")) !== false) {
            if (!isset($data[0])) {
                // empty line i the file
                continue;
            }

            $result = $this->getCommissionFee($data);
            fwrite(STDOUT, $result);
        }

        fclose($fileHandle);
    }

    protected function openCsv($filePath)
    {
        return fopen($filePath, "r");
    }

    private function getCommissionFee(array $data)
    {
        $account = $this->getAccount($data);
        if (!$account) {
            return 'Invalid transaction type!!';
        }

        $transactionDate = new DateTime($data[0]);
        $transactionType = $data[3];
        $transactionAmount = $data[4];
        $transactionCurrency = $data[5];

        switch ($transactionType) {
            case 'cash_in':
                $transaction = new CashInTransaction($transactionAmount, $transactionCurrency, $transactionDate);
                return $account->getCashInFee($transaction) . PHP_EOL;
                break;
            case 'cash_out':
                $transaction = new CashOutTransaction($transactionAmount, $transactionCurrency, $transactionDate);
                return $account->getCashOutFee($transaction) . PHP_EOL;
                break;
            default:
                break;
        }
    }

    private function getAccount(array $data)
    {
        $type = $data[2];
        $id = $data[1];
        if ($type == 'natural') {
            if (array_key_exists($id, $this->naturalAccounts)) {
                $account = $this->naturalAccounts[$id];
            } else {
                $account = new NaturalPersonAccount($id);
                $this->naturalAccounts[$id] = $account;
            }
        } elseif ($type == 'legal') {
            if (in_array($id, $this->legalAccounts)) {
                $account = $this->legalAccounts[$id];
            } else {
                $account = new LegalPersonAccount($id);
                $this->legalAccounts[$id] = $account;
            }
        } else {
            return false;
        }

        return $account;
    }
}






