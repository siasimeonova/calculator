<?php


namespace App\Account;

interface FeeAwareInterface
{

    /**
     *  Calculate fee to be applied on the requested amount.
     *
     * @param float $amount The requested payment amount for which need to be applied fee.
     * @param float $percent The commission percent of the amount.
     * @return double calculated fee amount
     */
    public function getFee(float $amount, float $percent): float;

}
