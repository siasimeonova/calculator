<?php


namespace App\Account;


use App\Transaction\AbstractTransaction;

interface TransactionFeeAwareInterface extends FeeAwareInterface
{

    /**
     *  Calculate fee to be applied on the requested sum.
     *
     * @param AbstractTransaction $transaction The requested transaction information for which amount need to be applied fee.
     * @return string calculated fee amount
     */
    public function getCashInFee(AbstractTransaction $transaction): string ;

    /**
     *  Calculate fee to be applied on the requested sum.
     *
     * @param AbstractTransaction $transaction The requested transaction information for which amount need to be applied fee.
     * @return string calculated fee amount
     */
    public function getCashOutFee(AbstractTransaction $transaction): string;

}
