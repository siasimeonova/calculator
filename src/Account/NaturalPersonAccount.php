<?php


namespace App\Account;

use App\CommissionRules\FeeAmountLimitationRule;
use App\CommissionRules\FreeOfChargeLimitRule;
use App\Converter\CurrencyConverter;
use App\Transaction\AbstractTransaction;
use App\Transaction\Currency;

class NaturalPersonAccount extends AbstractAccount
{
    protected $feeOfChargeRule;
    /**
     * @var int number of the week of the last processed transaction.
     */
    protected $lastTransactionWeek;

    /**
     * @var float Cached out amount this week in euro.
     */
    protected $cashedOutSumInEuroThisWeek = 0;
    /**
     * @var int Count of the check out transaction throw the current week.
     */
    protected $transactionCountThisWeek = 0;

    /**
     * @var CurrencyConverter
     */
    protected $currencyConverter;

    public function __construct(
        $id,
        FeeAmountLimitationRule $cashInCommissionRule = null,
        FeeAmountLimitationRule $cashOutCommissionRule = null
    ) {
        parent::__construct($id, $cashInCommissionRule, $cashOutCommissionRule);

        $this->setCashOutFeePercent(0.3);
        $this->feeOfChargeRule = new FreeOfChargeLimitRule();
        $this->currencyConverter = new CurrencyConverter();
    }

    /**
     * @param CurrencyConverter $currencyConverter
     */
    public function setCurrencyConverter(CurrencyConverter $currencyConverter): void
    {
        $this->currencyConverter = $currencyConverter;
    }

    /**
     *  Calculate fee to be applied on the requested sum.
     *
     * @param AbstractTransaction $transaction The requested transaction information for which amount need to be applied fee.
     * @return string calculated fee amount
     */
    public function getCashOutFee(AbstractTransaction $transaction): string
    {
        $transactionWeek = $transaction->getTime()->format('oW');
        if ($this->lastTransactionWeek != $transactionWeek) {
            $this->setNewTransactionWeek($transactionWeek);
        }

        $amountToBeCharged = $transaction->getAmount();
        $transactionCurrency = $transaction->getCurrency();
        if ($transactionCurrency === Currency::EURO) {
            $noFeeAmount = $this->feeOfChargeRule->getNoFeeAmount(
                $this->cashedOutSumInEuroThisWeek,
                $this->transactionCountThisWeek
            );

            $this->cashedOutSumInEuroThisWeek += $amountToBeCharged;

            $amountToBeCharged = ($noFeeAmount > $amountToBeCharged) ? 0 : $amountToBeCharged -= $noFeeAmount;
        } else {
            $amountToBeChargedEuro = $this->currencyConverter->convertCurrency(
                $transactionCurrency,
                Currency::EURO,
                $amountToBeCharged
            );

            $noFeeAmount = $this->feeOfChargeRule->getNoFeeAmount(
                $this->cashedOutSumInEuroThisWeek,
                $this->transactionCountThisWeek);
            $this->cashedOutSumInEuroThisWeek += $amountToBeChargedEuro;


            $amountToBeChargedEuro = ($noFeeAmount > $amountToBeChargedEuro) ? 0 : $amountToBeChargedEuro -= $noFeeAmount;

            if ($noFeeAmount > 0) {
                $amountToBeCharged = $this->currencyConverter->convertCurrency(
                    Currency::EURO,
                    $transactionCurrency,
                    $amountToBeChargedEuro
                );
            }
        }

        $this->transactionCountThisWeek++;

        if ($amountToBeCharged == 0) {
            $commissionFee = 0;

            $noMantissa = in_array($transaction->getCurrency(), AbstractTransaction::$noMantissaCurrencies);
            return $noMantissa ? ceil($commissionFee) : number_format($commissionFee, 2);
        }

        $transaction->setAmount($amountToBeCharged);
        return parent::getCashOutFee($transaction);
    }

    protected function setNewTransactionWeek($transactionWeek)
    {
        $this->lastTransactionWeek = $transactionWeek;
        $this->cashedOutSumInEuroThisWeek = 0;
        $this->transactionCountThisWeek = 0;
    }

    /**
     * @param mixed $checkedOutSumThisWeek
     */
    protected function addCheckedOutSumThisWeek($checkedOutSumThisWeek): void
    {
        $this->checkedOutSumThisWeek += $checkedOutSumThisWeek;
    }
}
