<?php


namespace App\Account;

use App\CommissionRules\FeeAmountLimitationRule;
use App\CommissionRules\MinFeeAmountRule;

class LegalPersonAccount extends AbstractAccount
{
    public function __construct(
        $id,
        FeeAmountLimitationRule $cashInCommissionRule = null,
        FeeAmountLimitationRule $cashOutCommissionRule = null
    ) {
        parent::__construct($id, $cashInCommissionRule, $cashOutCommissionRule);

        $this->setCashOutCommissionRule(new MinFeeAmountRule());
        $this->setCashOutFeePercent(0.3);
    }
}
