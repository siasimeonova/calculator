<?php


namespace App\Account;

use App\CommissionRules\FeeAmountLimitationRule;
use App\CommissionRules\MaxFeeAmountRule;
use App\Transaction\AbstractTransaction;
use InvalidArgumentException;

abstract class AbstractAccount implements TransactionFeeAwareInterface
{

    /**
     * @var float percent of the commission fee for check in transaction
     */
    protected $cashInFeePercent = 0.03;

    /**
     * @var float percent of the commission fee for check in transaction
     */
    protected $cashOutFeePercent;

    /**
     * @var FeeAmountLimitationRule
     */
    protected $cashInFeeAmountLimitationRule;

    /**
     * @var FeeAmountLimitationRule
     */
    protected $cashOutFeeAmountLimitationRule;
    protected $accountId;

    public function __construct(
        $id,
        FeeAmountLimitationRule $cashInCommissionRule = null,
        FeeAmountLimitationRule $cashOutCommissionRule = null
    ) {
        $this->accountId = $id;
        if (isset($cashInCommissionRule)) {
            $this->setCashInCommissionRule($cashInCommissionRule);
        } else {
            $this->setCashInCommissionRule(new MaxFeeAmountRule());
        }

        if (isset($cashOutCommissionRule)) {
            $this->setCashOutCommissionRule($cashOutCommissionRule);
        }
    }

    /**
     * @param FeeAmountLimitationRule $cashInCommissionRule
     */
    public function setCashInCommissionRule(FeeAmountLimitationRule $cashInCommissionRule): void
    {
        $this->cashInFeeAmountLimitationRule = $cashInCommissionRule;
    }

    /**
     * @param FeeAmountLimitationRule $cashOutCommissionRule
     */
    public function setCashOutCommissionRule(FeeAmountLimitationRule $cashOutCommissionRule): void
    {
        $this->cashOutFeeAmountLimitationRule = $cashOutCommissionRule;
    }

    /**
     * @param float $cashOutFeePercent
     */
    public function setCashOutFeePercent(float $cashOutFeePercent): void
    {
        if ($cashOutFeePercent < 0) {
            throw new InvalidArgumentException('Percent value must be a positive number!');
        }

        $this->cashOutFeePercent = $cashOutFeePercent;
    }

    /**
     * @param float $cashInFeePercent
     */
    public function setCashInFeePercent(float $cashInFeePercent): void
    {
        if ($cashInFeePercent < 0) {
            throw new InvalidArgumentException('Percent value must be a positive number!');
        }

        self::$cashInCommissionFeePercent = $cashInFeePercent;
    }

    /**
     *  Calculate fee to be applied on the requested sum.
     *
     * @param AbstractTransaction $transaction The requested transaction information for which amount need to be applied fee.
     * @return string calculated fee amount
     */
    public function getCashInFee(AbstractTransaction $transaction): string
    {
        $commissionFee = $this->getFee($transaction->getAmount(), $this->cashInFeePercent);

        if (isset($this->cashInFeeAmountLimitationRule)) {
            $commissionFee = $this->cashInFeeAmountLimitationRule->applyRule($commissionFee,
                $transaction->getCurrency());
        }

        $noMantissa = in_array($transaction->getCurrency(), AbstractTransaction::$noMantissaCurrencies);
        return $noMantissa ? ceil($commissionFee) : number_format($commissionFee, 2);
    }

    /**
     *  Calculate fee to be applied on the requested amount.
     *
     * @param float $amount The requested payment amount for which need to be applied fee.
     * @param float $percent The commission percent of the amount.
     * @return double calculated fee amount
     */
    public function getFee(float $amount, float $percent): float
    {
        return (ceil($amount * $percent)) / 100;
    }

    /**
     *  Calculate fee to be applied on the requested sum.
     *
     * @param AbstractTransaction $transaction The requested transaction information for which amount need to be applied fee.
     * @return string calculated fee amount
     */
    public function getCashOutFee(AbstractTransaction $transaction): string
    {
        $commissionFee = $this->getFee($transaction->getAmount(), $this->cashOutFeePercent);

        if (isset($this->cashOutFeeAmountLimitationRule)) {
            $commissionFee = $this->cashOutFeeAmountLimitationRule->applyRule(
                $commissionFee,
                $transaction->getCurrency()
            );
        }

        $noMantissa = in_array($transaction->getCurrency(), AbstractTransaction::$noMantissaCurrencies);
        return $noMantissa ? ceil($commissionFee) : number_format($commissionFee, 2);
    }

}
