<?php


namespace App\CommissionRules;


abstract class FeeAmountLimitationRule
{
    /**
     * @var float
     */
    protected $limitationAmount;

    abstract public function applyRule(float $amount, string $currency);

    /**
     * @param float $amount
     * @param string $currency
     */
    public function setFeeLimitationAmount(float $amount, string $currency): void
    {
        $this->limitationAmount[$currency] = $amount;
    }

    /**
     * @return mixed
     */
    public function deleteLimitationValue(string $key):void
    {
        unset($this->limitationAmount[$key]);
    }
}
