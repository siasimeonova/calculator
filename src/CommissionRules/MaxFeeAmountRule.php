<?php


namespace App\CommissionRules;

use App\Transaction\Currency;

class MaxFeeAmountRule extends FeeAmountLimitationRule
{

    protected $limitationAmount =
        [
            Currency::EURO => 5,
            Currency::JPY => 647.65,
            Currency::USD => 5.75,
        ];

    public function applyRule(float $amount, $currency)
    {
        return ($amount <= $this->limitationAmount[$currency]) ? $amount : $this->limitationAmount[$currency];
    }
}

