<?php


namespace App\CommissionRules;

use App\Transaction\Currency;

class MinFeeAmountRule extends FeeAmountLimitationRule
{

    protected $limitationAmount =
        [
            Currency::EURO => 0.50,
            Currency::USD => 0.58,
            Currency::JPY => 65
        ];

    public function applyRule(float $amount, string $currency)
    {
        return ($amount > $this->limitationAmount[$currency]) ? $amount : $this->limitationAmount[$currency];
    }
}

