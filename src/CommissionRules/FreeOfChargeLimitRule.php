<?php


namespace App\CommissionRules;

use InvalidArgumentException;

class FreeOfChargeLimitRule
{
    /**
     * @var float
     */
    private $weeklyNoFeeLimit = 1000;
    /**
     * @var int number of the week of the last processed transaction.
     */
    private $cashOutCountWithNoCommissionFee = 3;

    /**
     * @param int $weeklyNoFeeLimit
     */
    public function setWeeklyNoFeeLimit(int $weeklyNoFeeLimit): void
    {
        if ($weeklyNoFeeLimit < 1) {
            throw new InvalidArgumentException('Cash out count must be a positive number!');
        }
        self::$weeklyNoFeeLimit = $weeklyNoFeeLimit;
    }

    /**
     * @param int $cashOutCountWithNoCommissionFee
     */
    public function setCashOutCountWithNoCommissionFee(int $cashOutCountWithNoCommissionFee): void
    {
        if ($cashOutCountWithNoCommissionFee < 1) {
            throw new InvalidArgumentException('Cash out count must be a positive number!');
        }

        self::$cashOutCountWithNoCommissionFee = $cashOutCountWithNoCommissionFee;
    }

    /**
     * Return amount for cash out that is free of charge.
     *
     * @param float $cashedOutAmountThisWeek
     * @param int $cashOuts
     * @return float|int
     */
    public function getNoFeeAmount(float $cashedOutAmountThisWeek, int $cashOuts)
    {
        if ($cashOuts > $this->cashOutCountWithNoCommissionFee) {
            return 0;
        }

        if ($cashedOutAmountThisWeek >= $this->weeklyNoFeeLimit) {
            return 0;
        }

        return $this->weeklyNoFeeLimit - $cashedOutAmountThisWeek;
    }
}
