<?php


namespace App\Transaction;

class CashInTransaction extends AbstractTransaction
{
    /**
     * @var string type (direction) of the transaction.
     */
    protected static $type = TransactionTypes::CHECK_IN;

}
