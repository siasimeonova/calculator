<?php


namespace App\Transaction;

class CashOutTransaction extends AbstractTransaction
{
    /**
     * @var string type (direction) of the transaction.
     */
    protected static $type = TransactionTypes::CHECK_OUT;

}
