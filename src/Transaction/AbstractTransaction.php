<?php


namespace App\Transaction;

use DateTime;
use InvalidArgumentException;
use ReflectionException;

abstract class AbstractTransaction
{

    static $noMantissaCurrencies = [
        Currency::JPY
    ];
    /**
     * @var string type (direction) of the transaction.
     */
    protected static $type;
    /**
     * @var float Amount for the transaction.
     */
    protected $amount;
    /**
     * @var string The currency of the transaction.
     */
    protected $currency;
    /**
     * @var DateTime The time of the transaction.
     */
    protected $time;

    public function __construct(float $amount, string $currency, DateTime $date)
    {
        $this->setAmount($amount);
        $this->setCurrency($currency);
        $this->setTime($date);
    }

    /**
     * @return string
     */
    public static function getType(): string
    {
        return self::$type;
    }

    /**
     * @param array $noMantissaCurrencies
     */
    public static function setNoMantissaCurrencies(array $noMantissaCurrencies): void
    {
        self::$noMantissaCurrencies = $noMantissaCurrencies;
    }

    /**
     * @param string $currency
     */
    public static function addNoMantissaCurrency(string $currency): void
    {
        self::$noMantissaCurrencies[] = $currency;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        if ($amount < 0) {
            throw new InvalidArgumentException('Transaction amount can be only a positive number!');
        }
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @throws ReflectionException, InvalidArgumentException
     */
    public function setCurrency(string $currency): void
    {
        if (!Currency::isValidValue($currency)) {
            throw new InvalidArgumentException('Invalid or not supported currency!');
        }

        $this->currency = $currency;
    }

    /**
     * @return DateTime
     */
    public function getTime(): DateTime
    {
        return $this->time;
    }

    /**
     * @param DateTime $time
     */
    public function setTime(DateTime $time): void
    {
        $this->time = $time;
    }
}

