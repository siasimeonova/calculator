<?php


namespace App\Transaction;

/**
 * Enum class with transaction types
 *
 * Class TransactionTypes
 * @package App\Transaction
 */
final class TransactionTypes
{

    const CHECK_IN = 'check-in';
    const CHECK_OUT = 'check-out';

    static public $types = [
        self::CHECK_IN,
        self::CHECK_OUT,
    ];

    private function __construct()
    {
    }

    /**
     * Validate existence of searched value.
     *
     * @param $value
     * @return bool
     */
    public static function isValidValue($value)
    {
        return in_array($value, static::$types);
    }
}
