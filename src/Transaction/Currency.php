<?php


namespace App\Transaction;

/**
 * Enum class with currency types.
 *
 * Class Currency
 * @package App\Transaction
 */
final class Currency
{
    const EURO = 'EUR';
    const USD = 'USD';
    const JPY = 'JPY';

    static public $currencies = [
        self::EURO => 'EUR',
        self::USD => 'USD',
        self::JPY => 'JPY',
    ];

    private function __construct()
    {
    }

    /**
     * Validate existence of searched value.
     *
     * @param $value
     * @return bool
     */
    public static function isValidValue($value)
    {
        return key_exists($value, static::$currencies);
    }

}
