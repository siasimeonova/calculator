<?php


namespace App\Converter;

interface ConvertAwareInterface
{


    /**
     * Calculate the value of the converted amount
     *
     * @param float $value Amount to be converted
     * @return float
     */
    public function convert(float $value): float;

}
