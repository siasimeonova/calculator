<?php


namespace App\Converter;

use App\Transaction\Currency;
use InvalidArgumentException;

class CurrencyConverter implements ConvertAwareInterface
{

    protected $course;

    /*
     * array
     */
    protected $converters = [
        Currency::EURO => [
            Currency::JPY => EuroToJpyConverter::class,
            Currency::USD => EuroToDollarConverter::class
        ],
        Currency::USD => [Currency::EURO => UsdToEurConverter::class],
        Currency::JPY => [Currency::EURO => JpyToEurConverter::class],
    ];

    public function convertCurrency(string $currencyFrom, string $currencyTo, float $amount): float
    {
        if (!isset($this->converters[$currencyFrom]) || !isset($this->converters[$currencyFrom][$currencyTo])) {
            throw new InvalidArgumentException('Invalid currency provided!');
        }

        $converter = new $this->converters[$currencyFrom][$currencyTo]();

        if (!$converter) {
            throw new InvalidArgumentException('Invalid currency provided!');
        }
        return $converter->convert($amount);
    }

    /**
     * @param array $converters
     */
    public function setConverters(array $converters): void
    {
        $this->converters = $converters;
    }

    /**
     * @param string $currency
     * @param array $currencyConverters
     */
    public function addConverter(string $currency, array $currencyConverters): void
    {
        $this->converters[$currency] = $currencyConverters;
    }

    /**
     * Calculate the value of the converted amount
     *
     * @param $value
     * @return float
     */
    public function convert(float $value): float
    {
        return (ceil($value * 100 * $this->course)) / 100;
    }

    /**
     * @param float $course
     */
    public function setCourse(float $course): void
    {
        if (!$course || $course < 0) {
            throw new InvalidArgumentException('Invalid course!');
        }

        $this->course = $course;
    }
}
