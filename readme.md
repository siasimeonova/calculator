# Module for calculation of commission fees.

The purpouse of the module is to handle commission fee management, including currency convertion if needed.

## Installation

Run composer install


## Run unit tests

./bin/phpunit

## Run calculator with test data

In bin folder:

php commission_fee_calculator.php {path to csv file with data}
