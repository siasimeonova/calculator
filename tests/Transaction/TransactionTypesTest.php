<?php


namespace App\Tests\Transaction;


use App\Transaction\TransactionTypes;
use PHPUnit\Framework\TestCase;

class TransactionTypesTest extends TestCase
{


    /**
     * @param $testValue
     * @param $expected
     * @dataProvider provider_for_test_is_valid_value
     */
    public function test_is_valid_value($testValue, $expected)
    {

        try{
            $result = TransactionTypes::isValidValue($testValue);
        }catch (\ReflectionException $ex)
        {
            return $ex->getMessage();
        }

        $this->assertEquals($expected, $result);
    }

    public function provider_for_test_is_valid_value()
    {
        return [
            'check_out' => [TransactionTypes::CHECK_OUT,true],
            'check_in' => [TransactionTypes::CHECK_IN,true],
            'empty_string' => ['',false],
            'random_number' => ['123',false],
            'random_string' => ['retge356v vf',false],
            ];
    }

}