<?php


namespace App\Tests\Transaction;


use App\Transaction\Currency;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class CurrencyTest extends TestCase
{


    /**
     * @param $testValue
     * @param $expected
     * @dataProvider provider_for_test_is_valid_value
     *
     */
    public function test_is_valid_value($testValue, $expected)
    {

        try {
            $result = Currency::isValidValue($testValue);
        } catch (ReflectionException $ex) {
            return $ex->getMessage();
        }

        $this->assertEquals($expected, $result);
    }

    public function provider_for_test_is_valid_value()
    {
        return [
            'eur' => [Currency::EURO, true],
            'usd' => [Currency::USD, true],
            'jpg' => [Currency::JPY, true],
            'bgn' => ['BGN', false],
            'empty_string' => ['', false],
            'random_number' => ['123', false],
            'random_string' => ['retge356v vf', false],
        ];
    }

}