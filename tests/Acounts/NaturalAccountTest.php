<?php


namespace App\Tests\Acounts;


use App\Account\NaturalPersonAccount;
use App\Transaction\CashInTransaction;
use App\Transaction\Currency;
use DateTime;
use PHPUnit\Framework\TestCase;

class NaturalAccountTest extends TestCase
{

    /**
     * @param $transaction
     * @param $expectedFee
     * @dataProvider provider_for_test_get_check_in_fee
     */
    public function test_get_check_in_fee($transaction, $expectedFee, $id)
    {

        $accounts = [
            new NaturalPersonAccount(1),
            new NaturalPersonAccount(3),
            new NaturalPersonAccount(4),
            new NaturalPersonAccount(5),
        ];

        $fee = $accounts[$id]->getCashInFee($transaction);

        $this->assertEquals($expectedFee, $fee);
    }

    public function provider_for_test_get_check_in_fee()
    {
        $date = new DateTime('2016-01-05');
        $CashInTransaction1 = new CashInTransaction(2000000.00, Currency::EURO, $date);
        $CashInTransaction2 = new CashInTransaction(2000000.00, Currency::JPY, $date);
        $CashInTransaction3 = new CashInTransaction(2000000.00, Currency::USD, $date);
        $CashInTransaction4 = new CashInTransaction(300.00, Currency::EURO, $date);
        $CashInTransaction5 = new CashInTransaction(300.00, Currency::JPY, $date);
        $CashInTransaction6 = new CashInTransaction(300.00, Currency::USD, $date);

        return [
            'euro_above_max_return_max' => [$CashInTransaction1, 5.00, 1],
            'jpy_above_max_return_max' => [$CashInTransaction2, 600.00, 1],
            'usd_above_max_return_max' => [$CashInTransaction3, 5.75, 1],
            'euro_bellow_max_return_calculated' => [$CashInTransaction4, 0.09, 1],
            'jpy_bellow_max_return_calculated' => [$CashInTransaction5, 1, 1],
            'usd_bellow_max_return_calculated' => [$CashInTransaction6, 0.09, 1]
        ];

    }

    public function test_get_check_out_fee_person_4()
    {
        $account = new NaturalPersonAccount(4);

        $CashOutTransaction1 = new CashInTransaction(1200.00, Currency::EURO, new DateTime('2014-12-31'));
        $CashOutTransaction2 = new CashInTransaction(1000.00, Currency::EURO, new DateTime('2015-01-01'));
        $CashOutTransaction3 = new CashInTransaction(1000.00, Currency::EURO, new DateTime('2016-01-05'));
        $fee1 = $account->getCashOutFee($CashOutTransaction1);
        $this->assertEquals(0.60, $fee1);

        $fee2 = $account->getCashOutFee($CashOutTransaction2);
        $this->assertEquals(3.00, $fee2);

        $fee3 = $account->getCashOutFee($CashOutTransaction3);
        $this->assertEquals(0.00, $fee3);
    }

    public function test_get_check_out_fee_person_1()
    {
        $account = new NaturalPersonAccount(1);

        $CashOutTransaction1 = new CashInTransaction(30000.00, Currency::JPY, new DateTime('2016-01-06'));
        $CashOutTransaction2 = new CashInTransaction(1000.00, Currency::EURO, new DateTime('2016-01-07'));
        $CashOutTransaction3 = new CashInTransaction(100.00, Currency::USD, new DateTime('2016-01-07'));
        $CashOutTransaction4 = new CashInTransaction(100.00, Currency::EURO, new DateTime('2016-01-10'));
        $CashOutTransaction5 = new CashInTransaction(300.00, Currency::EURO, new DateTime('2016-02-15'));

        $fee1 = $account->getCashOutFee($CashOutTransaction1);
        $this->assertEquals(0.00, $fee1);

        $fee2 = $account->getCashOutFee($CashOutTransaction2);
        $this->assertEquals(0.70, $fee2);

        $fee3 = $account->getCashOutFee($CashOutTransaction3);
        $this->assertEquals(0.30, $fee3);

        $fee4 = $account->getCashOutFee($CashOutTransaction4);
        $this->assertEquals(0.30, $fee4);

        $fee5 = $account->getCashOutFee($CashOutTransaction5);
        $this->assertEquals(0.00, $fee5);
    }

    public function test_get_check_out_fee_person_3()
    {
        $account = new NaturalPersonAccount(1);

        $CashOutTransaction1 = new CashInTransaction(1000.00, Currency::EURO, new DateTime('2016-01-06'));

        $fee1 = $account->getCashOutFee($CashOutTransaction1);
        $this->assertEquals(0.00, $fee1);
    }

    public function test_get_check_out_fee_person_5()
    {
        $account = new NaturalPersonAccount(5);

        $CashOutTransaction1 = new CashInTransaction(3000000.00, Currency::JPY, new DateTime('2016-01-06'));

        $fee1 = $account->getCashOutFee($CashOutTransaction1);
        $this->assertEquals(8612, $fee1);
    }

}
