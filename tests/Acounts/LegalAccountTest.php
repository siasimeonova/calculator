<?php


namespace App\Tests\Acounts;


use App\Account\LegalPersonAccount;
use App\Transaction\CashInTransaction;
use App\Transaction\CashOutTransaction;
use App\Transaction\Currency;
use PHPUnit\Framework\TestCase;

class LegalAccountTest extends TestCase
{

    /**
     * @param $transaction
     * @param $expectedFee
     * @throws \ReflectionException
     * @dataProvider provider_for_test_get_check_in_fee
     */
    public function test_get_check_in_fee($transaction, $expectedFee)
    {
        $account = new LegalPersonAccount(1);
        $fee = $account->getCashInFee($transaction);

        $this->assertEquals($expectedFee, $fee);
    }

    public function provider_for_test_get_check_in_fee()
    {
        $date = new \DateTime('2014-12-31');
        $CashInTransaction1 = new CashInTransaction(10000000.00, Currency::EURO,$date);
        $CashInTransaction2 = new CashInTransaction(10000000.00, Currency::JPY, $date);
        $CashInTransaction3 = new CashInTransaction(10000000.00, Currency::USD, $date);
        $CashInTransaction4 = new CashInTransaction(100.00, Currency::EURO, $date);
        $CashInTransaction5 = new CashInTransaction(100.00, Currency::JPY, $date);
        $CashInTransaction6 = new CashInTransaction(100.00, Currency::USD, $date);

        return [
                'euro_above_max_return_max' => [$CashInTransaction1, 5.00],
                'jpy_above_max_return_max' => [$CashInTransaction2, 648],
                'usd_above_max_return_max' => [$CashInTransaction3, 5.75],
                'euro_bellow_max_return_calculated' => [$CashInTransaction4, 0.03],
                'jpy_bellow_max_return_calculated' => [$CashInTransaction5, 1],
                'usd_bellow_max_return_calculated' => [$CashInTransaction6, 0.03]
        ];

    }


    /**
     * @param $transaction
     * @param $expectedFee
     * @throws \ReflectionException
     * @dataProvider provider_for_test_get_check_out_fee
     */
    public function test_get_check_out_fee($transaction, $expectedFee)
    {
        $account = new LegalPersonAccount(1);
        $fee = $account->getCashOutFee($transaction);

        $this->assertEquals($expectedFee, $fee);
    }

    public function provider_for_test_get_check_out_fee()
    {
        $date = new \DateTime('2014-12-31');
        $CashOutTransaction1 = new CashOutTransaction(30000.00, Currency::EURO, $date);
        $CashOutTransaction2 = new CashOutTransaction(30000.00, Currency::JPY, $date);
        $CashOutTransaction3 = new CashOutTransaction(30000.00, Currency::USD, $date);
        $CashOutTransaction4 = new CashOutTransaction(1.00, Currency::EURO, $date);
        $CashOutTransaction5 = new CashOutTransaction(1.00, Currency::JPY, $date);
        $CashOutTransaction6 = new CashOutTransaction(1.00, Currency::USD, $date);

        return [
            'euro_above_min_return_calculated' => [$CashOutTransaction1, 90.00],
            'jpy_above_min_return_calculated' => [$CashOutTransaction2, 90.00],
            'usd_above_min_return_calculated' => [$CashOutTransaction3, 90.00],
            'euro_bellow_min_return_min' => [$CashOutTransaction4, 0.50],
            'jpy_bellow_min_return_min' => [$CashOutTransaction5, 65],
            'usd_bellow_min_return_min' => [$CashOutTransaction6, 0.58]
        ];

    }


}